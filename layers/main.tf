module "azurerm" {
  source = "../modules"

  name             = var.name
  location         = var.location
  application_type = var.application_type
  sku_kv           = var.sku_kv
  sku_cr           = var.sku_cr
  sku_pbi          = var.sku_pbi
  admin_pbi        = var.admin_pbi
}