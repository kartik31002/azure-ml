variable "name" {
  description = "Name to be used on all resources as prefix"
  #default     = "anpr"
}

variable "location" {
  description = "Location of all resources"
  #default     = "Central India"
}

variable "application_type" {
  description = "type of application i.e. ios, java, Node.JS, web etc."
  #default     = "web"
}

variable "sku_kv" {
  description = "Type of SKU for Key Vault"
  #default     = "standard"

}

variable "sku_cr" {
  description = "Type of SKU for Container Registry"
  #default     = "Basic"

}

variable "sku_pbi" {
  description = "Type of SKU for PowerBI"
  #default     = "A1"

}

variable "admin_pbi" {
  description = "Admin for PowerBI"
  default = ["kartik31002@gmail.com"]
}
