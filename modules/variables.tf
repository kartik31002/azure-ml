variable "name" {
  description = "Name to be used on all resources as prefix"
  type     = string
}

variable "location" {
  description = "Location of all resources"
  type = string
}

variable "application_type" {
  description = "type of application i.e. ios, java, Node.JS, web etc."
  type = string
}

variable "sku_kv" {
  description = "Type of SKU for Key Vault"
  type = string
}

variable "sku_cr" {
  description = "Type of SKU for Container Registry"
  type = string
}

variable "sku_pbi" {
  description = "Type of SKU for PowerBI"
  type = string
}

variable "admin_pbi" {
  description = "Admin for PowerBI"
  type = list(string)
}