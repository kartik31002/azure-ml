# provider "azurerm" {
#   features {}
# }
resource "random_id" "randid"{
  byte_length = 8
}
data "azurerm_client_config" "current" {}

resource "azurerm_resource_group" "example" {
  name     = var.name
  location = var.location
}

resource "azurerm_application_insights" "app-insights" {
  name                = "${var.name}-app-insights"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  application_type    = var.application_type
}

resource "azurerm_key_vault" "keyvault" {
  name                = "${var.name}-kv${random_id.randid.id}"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  tenant_id           = data.azurerm_client_config.current.tenant_id
  sku_name            = var.sku_kv
}

resource "azurerm_storage_account" "storage" {
  name                     = "${var.name}storageaccount"
  location                 = azurerm_resource_group.example.location
  resource_group_name      = azurerm_resource_group.example.name
  account_tier             = "Standard"
  account_replication_type = "GRS"
}

resource "azurerm_container_registry" "acr" {
  name                = "${var.name}ContainerRegistry"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  sku                 = var.sku_cr
  admin_enabled       = true
}

resource "azurerm_machine_learning_workspace" "workspace" {
  name                    = "${var.name}-workspace"
  location                = azurerm_resource_group.example.location
  resource_group_name     = azurerm_resource_group.example.name
  application_insights_id = azurerm_application_insights.app-insights.id
  key_vault_id            = azurerm_key_vault.keyvault.id
  storage_account_id      = azurerm_storage_account.storage.id
  container_registry_id   = azurerm_container_registry.acr.id

  identity {
    type = "SystemAssigned"
  }
}


resource "azurerm_data_factory" "datafactory" {
  name                = "${var.name}-datafactory"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
}

resource "azurerm_data_factory_linked_service_azure_blob_storage" "blob" {
  name              = "${var.name}-blob"
  data_factory_id   = azurerm_data_factory.datafactory.id
  connection_string = azurerm_storage_account.storage.primary_connection_string
}

#resource "azurerm_powerbi_embedded" "example" {
#  name                = "${var.name}powerbi"
#  location            = azurerm_resource_group.example.location
#  resource_group_name = azurerm_resource_group.example.name
#  sku_name            = var.sku_pbi
#  administrators      = var.admin_pbi
#}