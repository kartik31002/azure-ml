################################################
#makefile

LOCATION               = layers
STATEBUCKET            = new-terraform-bucket
STATEREGION            = us-east-1
STATEKEY               = terraform/azureml.tfstate

ifndef LOCATION
$(error LOCATION is not set)
endif

.PHONY: plan

first-run:
	@echo "initialize terraform statefile"
	cd $(LOCATION) && \
	rm -r .terraform/ || true && \
	export ARM_SUBSCRIPTION_ID=$(ARM_SUBSCRIPTION_ID) && \
    export ARM_TENANT_ID=$(ARM_TENANT_ID) && \
    export ARM_CLIENT_ID=$(ARM_CLIENT_ID) && \
    export ARM_CLIENT_SECRET=$(ARM_CLIENT_SECRET) && \
	export AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID) && \
    export AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY) && \
    export TF_VAR_name=$(name) TF_VAR_location=$(location) TF_VAR_application_type=$(application_type) TF_VAR_sku_kv=$(sku_kv) TF_VAR_sku_cr=$(sku_cr) TF_VAR_sku_pbi=$(sku_pbi)

init:
	@echo "initialize terraform statefile and workspace"
	cd $(LOCATION) && \
    export ARM_SUBSCRIPTION_ID=$(ARM_SUBSCRIPTION_ID) && \
    export ARM_TENANT_ID=$(ARM_TENANT_ID) && \
    export ARM_CLIENT_ID=$(ARM_CLIENT_ID) && \
    export ARM_CLIENT_SECRET=$(ARM_CLIENT_SECRET) && \
	export AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID) && \
    export AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY) && \
    export TF_VAR_name=$(name) TF_VAR_location=$(location) TF_VAR_application_type=$(application_type) TF_VAR_sku_kv=$(sku_kv) TF_VAR_sku_cr=$(sku_cr) TF_VAR_sku_pbi=$(sku_pbi) && \
    terraform init -upgrade -reconfigure -backend-config="bucket=$(STATEBUCKET)" -backend-config="key=$(STATEKEY)" -backend-config="region=$(STATEREGION)"

validate: init
	@echo "running terraform validate"
	cd $(LOCATION) && \
    terraform validate -no-color

plan: validate
	@echo "running terraform plan"
	cd $(LOCATION) && \
	export ARM_SUBSCRIPTION_ID=$(ARM_SUBSCRIPTION_ID) && \
    export ARM_TENANT_ID=$(ARM_TENANT_ID) && \
    export ARM_CLIENT_ID=$(ARM_CLIENT_ID) && \
    export ARM_CLIENT_SECRET=$(ARM_CLIENT_SECRET) && \
    export TF_VAR_name=$(name) TF_VAR_location=$(location) TF_VAR_application_type=$(application_type) TF_VAR_sku_kv=$(sku_kv) TF_VAR_sku_cr=$(sku_cr) TF_VAR_sku_pbi=$(sku_pbi) && \
    terraform plan -no-color
	@echo "plan completed"

apply: plan
	@echo "running terraform apply"
	cd $(LOCATION) && \
    export ARM_SUBSCRIPTION_ID=$(ARM_SUBSCRIPTION_ID) && \
    export ARM_TENANT_ID=$(ARM_TENANT_ID) && \
    export ARM_CLIENT_ID=$(ARM_CLIENT_ID) && \
    export ARM_CLIENT_SECRET=$(ARM_CLIENT_SECRET) && \
    export TF_VAR_name=$(name) TF_VAR_location=$(location) TF_VAR_application_type=$(application_type) TF_VAR_sku_kv=$(sku_kv) TF_VAR_sku_cr=$(sku_cr) TF_VAR_sku_pbi=$(sku_pbi) && \
    terraform apply  -auto-approve -no-color

plan-destroy: validate
	@echo "running terraform plan-destroy"
	cd $(LOCATION) && \
    export ARM_SUBSCRIPTION_ID=$(ARM_SUBSCRIPTION_ID) && \
    export ARM_TENANT_ID=$(ARM_TENANT_ID) && \
    export ARM_CLIENT_ID=$(ARM_CLIENT_ID) && \
	export ARM_CLIENT_SECRET=$(ARM_CLIENT_SECRET) && \
    export TF_VAR_name=$(name) TF_VAR_location=$(location) TF_VAR_application_type=$(application_type) TF_VAR_sku_kv=$(sku_kv) TF_VAR_sku_cr=$(sku_cr) TF_VAR_sku_pbi=$(sku_pbi) && \
    terraform plan -destroy -no-color

destroy: init
	@echo "running terraform destroy"
	cd $(LOCATION) && \
	export ARM_SUBSCRIPTION_ID=$(ARM_SUBSCRIPTION_ID) && \
    export ARM_TENANT_ID=$(ARM_TENANT_ID) && \
    export ARM_CLIENT_ID=$(ARM_CLIENT_ID) && \
    export ARM_CLIENT_SECRET=$(ARM_CLIENT_SECRET) && \
    export TF_VAR_name=$(name) TF_VAR_location=$(location) TF_VAR_application_type=$(application_type) TF_VAR_sku_kv=$(sku_kv) TF_VAR_sku_cr=$(sku_cr) TF_VAR_sku_pbi=$(sku_pbi) && \
    terraform destroy -auto-approve -no-color